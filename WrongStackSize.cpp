#include "WrongStackSize.h"

WrongStackSize::WrongStackSize() : reason("Wrong Stack Size") {}

const char *WrongStackSize::what() const
{
	return this->reason;
}