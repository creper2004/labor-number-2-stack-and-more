#pragma once
#include <exception>

class WrongStackSize : public std::exception
{
private:
	const char* reason;
public:
	WrongStackSize();
	const char* what() const;
};