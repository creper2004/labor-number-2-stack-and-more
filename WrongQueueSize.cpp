#include "WrongQueueSize.h"

WrongQueueSize::WrongQueueSize() : reason("Wrong Queue Size") {}

const char *WrongQueueSize::what() const
{
	return this->reason;
}