﻿#include <iostream>
#include "QueueArray.h"

int main()
{
	try
	{
		QueueArray<int> q(3949094559505);
	}
	catch (const WrongQueueSize& e)
	{
		std::cerr << e.what() << "\n";
	}

	QueueArray<int> qfull(2);
	qfull.enQueue(10);
	qfull.enQueue(8);
	try
	{
		qfull.enQueue(5);
	}
	catch (const QueueOverflow& e)
	{
		std::cerr << e.what() << "\n";
	}

	QueueArray<int> qemp(5);
	try
	{
		qemp.deQueue();
	}
	catch (const QueueUnderflow& e)
	{
		std::cerr << e.what() << "\n";
	}
	std::cout << "\n";

	QueueArray<int> que(7);
	que.enQueue(135);
	que.enQueue(15);
	que.enQueue(55);
	que.enQueue(189);
	que.enQueue(112);
	que.enQueue(93);
	que.enQueue(73);

	int cnt = que.getCount();
	for (int i = 0; i < cnt; i++)
	{
		std::cout << que.deQueue() << "\n";
	}

	std::cout << "\n";
	if (que.isEmpty())
	{
		std::cout << "Queue is empty\n";
	}
	else
	{
		std::cout << "Queue is not empty\n";
	}

	return 0;

}