#include "StackOverflow.h"

StackOverflow::StackOverflow() : reason("Stack Overflow") {}

const char *StackOverflow::what() const
{
	return this->reason;
}
