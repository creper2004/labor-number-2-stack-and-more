#include "QueueOverflow.h"

QueueOverflow::QueueOverflow() : reason("Queue Overflow") {}

const char *QueueOverflow::what() const
{
	return this->reason;
}