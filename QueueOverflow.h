#pragma once
#include <exception>

class QueueOverflow : public std::exception
{
private:
	const char* reason;
public:
	QueueOverflow();
	const char* what() const;
};