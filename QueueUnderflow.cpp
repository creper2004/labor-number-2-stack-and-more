#include "QueueUnderflow.h"

QueueUnderflow::QueueUnderflow() : reason("Queue Underflow") {}

const char *QueueUnderflow::what() const
{
	return this->reason;
}