#pragma once
#include <exception>

class StackUnderwlow : public std::exception
{
private:
	const char* reason;
public:
	StackUnderwlow();
	const char* what() const;
};