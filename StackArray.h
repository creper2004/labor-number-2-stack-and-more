#pragma once
#include "Stack.h"
#include "StackOverflow.h"
#include "StackUnderflow.h"
#include "WrongStackSize.h"

template <class T>
class StackArray : public Stack<T>
{
public:
	StackArray(size_t size = 100);
	StackArray(const StackArray<T>& src);
	StackArray(StackArray<T>&& src);
	StackArray& operator=(const StackArray<T>& src) = delete;
	StackArray& operator=(StackArray<T>&& src);
	virtual ~StackArray();

	void push(const T& e) override;
	T pop() override;
	bool isEmpty() override;

private:
	T* array_;
	size_t top_;
	size_t size_;
	void swap(StackArray<T>& src);
};


template <class T>
StackArray<T>::StackArray(size_t size)
{
	size_ = size;
	top_ = 0;
	try
	{
		array_ = new T[size + 1];
	}
	catch (const std::bad_array_new_length& e)
	{
		throw WrongStackSize();
	}
}

template<class T>
StackArray<T>::StackArray(const StackArray<T>& src)
{
	size_(src.size_);
	top_(src.top_);
	try 
	{
		array_ = new T[src.size_ + 1];
	}
	catch (const std::bad_array_new_length& e)
	{
		throw WrongStackSize();
	}
	for (size_t i = 1; i < src.top_; i++) 
	{
		array_[i] = src.array_[i];
	}
}

template<class T>
StackArray<T>::StackArray(StackArray<T>&& src)
{
	size_ = src.size_;
	top_ = src.top_;
	array_ = src.array_;

	src.size_ = 0;
	src.top_ = 0;
	src.array_ = nullptr;
}

template<class T>
StackArray<T>& StackArray<T>::operator=(StackArray<T>&& src)
{
	if (this != &src)
	{
		swap(src);
	}
	return *this;
}

template<class T>
StackArray<T>::~StackArray()
{
	delete[] array_;
}

template<class T>
void StackArray<T>::push(const T& e)
{
	if (top_ == size_)
	{
		throw StackOverflow();
	}
	array_[++top_] = e;
}

template<class T>
T StackArray<T>::pop()
{
	if (isEmpty())
	{
		throw StackUnderwlow();
	}
	return array_[top_--];;
}

template<class T>
bool StackArray<T>::isEmpty()
{
	return top_ == 0;
}

template <class T>
void StackArray<T>::swap(StackArray<T>& src)
{
	std::swap(array_, src.array_);
	std::swap(top_, src.top_);
	std::swap(size_, src.size_);
}