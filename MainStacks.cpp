﻿#include <iostream>
#include "StackArray.h"

bool checkBalanceBrackets(const char* text, const int maxDeep);
const int SIZE = 8;

int main()
{
	try
	{
		StackArray<int> bigstack(20490909509309505);
	}
	catch (const WrongStackSize & e)
	{
		std::cerr << e.what() << ". Try to change size of stack\n";
	}

	StackArray<int> willemp(1);
	willemp.push(312);
	willemp.pop();
	try
	{
		willemp.pop();
	}
	catch (const StackUnderwlow& e)
	{
		std::cerr << e.what() << ". Stack is empty yet and you try to remove element \n";
	}

	StackArray<int> willfull(1);
	willfull.push(90);
	try
	{
		willfull.push(200);
	}
	catch (const StackOverflow& e)
	{
		std::cerr << e.what() << ". Stack is full yet and you try to add element \n";
	}
	std::cout << "\n";

	StackArray<int> stk(5);
	stk.push(90);
	stk.push(12);
	stk.push(123);
	stk.push(17);
	stk.push(43);

	for (int i = 0; i < 5; i++)
	{
		std::cout << stk.pop() << "\n";
	}

	if (stk.isEmpty())
	{
		std::cout << "Stack is empty\n";
	}
	else
	{
		std::cout << "Stack is not empty\n";
	}
	std::cout << "\n";

	const char* many[SIZE] = { "(25-{3-5}*[10*7])*(2*10)", "(25+(33*22))[10]", "{90-(2+5)+[2+3]}", "[[[[[]]]", "()))", "({([})", "()[]{}", "90" };
	for (int i = 0; i < SIZE; i++)
	{
		if (checkBalanceBrackets(many[i], 100))
		{
			std::cout << many[i] << "  is TRUE brackets\n";
		}
		else
		{
			std::cout << many[i] << "  is FALSE brackets\n";
		}
	}

	return 0;

}

bool checkBalanceBrackets(const char * text, const int maxDeep)
{
	StackArray<char> stack(maxDeep);
	int i = 0;
	while (text[i] != '\0')
	{
		if (text[i] == '(' || text[i] == '[' || text[i] == '{')
		{
			stack.push(text[i]);
			i++;
			continue;
		}
		if (text[i] == ')' || text[i] == ']' || text[i] == '}')
		{
			if (stack.isEmpty())
			{
				return false;
			}
			char cur_open_bracket = stack.pop();
			if (cur_open_bracket == '(' && text[i] == ')')
			{
				i++;
				continue;
			}
			if (cur_open_bracket == '[' && text[i] == ']')
			{
				i++;
				continue;
			}
			if (cur_open_bracket == '{' && text[i] == '}')
			{
				i++;
				continue;
			}
			return false;
		}
		else
		{
			i++;
		}
	}
	if (stack.isEmpty())
	{
		return true;
	}
	return false;
}