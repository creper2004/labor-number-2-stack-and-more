#pragma once
#include <exception>

class QueueUnderflow : public std::exception
{
private:
	const char* reason;
public:
	QueueUnderflow();
	const char* what() const;
};