#pragma once
#include "Queue.h"
#include "QueueOverflow.h"
#include "QueueUnderflow.h"
#include "WrongQueueSize.h"

template <class T>
class QueueArray : public Queue<T>
{
private:
	size_t front;
	size_t end;
	size_t size_;
	int count;
	T* array_;
	void swap(QueueArray<T>& src);
public:
	QueueArray(size_t size = 100);
	QueueArray(const QueueArray<T>& src);
	QueueArray(QueueArray<T>&& src);
	QueueArray& operator=(const QueueArray<T>& src) = delete;
	QueueArray& operator=(QueueArray<T>&& src);
	virtual ~QueueArray();

	void enQueue(const T& e) override;
	T deQueue() override;
	bool isEmpty() override;
	int getCount();
};

template<class T>
QueueArray<T>::QueueArray(size_t size)
{
	size_ = size;
	front = 0;
	end = 0;
	count = 0;
	try
	{
		array_ = new T[size + 1];
	}
	catch (const std::bad_array_new_length& e)
	{
		throw WrongQueueSize();
	}
}

template<class T>
QueueArray<T>::QueueArray(const QueueArray<T>& src)
{
	size_(src.size_);
	front(src.front);
	end(src.end);
	count(src.count);
	try
	{
		array_ = new T[src.size_ + 1];
	}
	catch (const std::bad_array_new_length& e)
	{
		throw WrongQueueSize();
	}
	for (size_t i = 1; i < src.end; i++)
	{
		array_[i] = src.array_[i];
	}
}

template<class T>
QueueArray<T>::QueueArray(QueueArray<T>&& src)
{
	size_ = src.size_;
	front = src.front;
	end = src.end;
	array_ = src.array_;
	count = src.count;

	src.size_ = 0;
	src.front = 0;
	src.end = 0;
	src.count = 0;
	src.array_ = nullptr;
}

template<class T>
QueueArray<T>& QueueArray<T>::operator=(QueueArray<T>&& src)
{
	if (this != &src)
	{
		swap(src);
	}
	return *this;
}

template<class T>
QueueArray<T>::~QueueArray()
{
	delete[] array_;
}

template<class T>
void QueueArray<T>::swap(QueueArray<T>& src)
{
	std::swap(array_, src.array_);
	std::swap(front, src.front);
	std::swap(end, src.end);
	std::swap(count, src.count);
	std::swap(size_, src.size_);
}

template<class T>
bool QueueArray<T>::isEmpty()
{
	return (front == 0 && end == 0 && count == 0);
}

template<class T>
int QueueArray<T>::getCount()
{
	return this->count;
}

template<class T>
void QueueArray<T>::enQueue(const T & e)
{
	if (end == size_)
	{
		throw QueueOverflow();
	}
	if (isEmpty())
	{
		array_[++end] = e;
		front++;
		count++;
	}
	else
	{
		array_[++end] = e;
		count++;
	}
}

template<class T>
T QueueArray<T>::deQueue()
{
	if (isEmpty())
	{
		throw QueueUnderflow();
	}
	if (front == end)
	{
		T temp = array_[front];
		front = 0;
		end = 0;
		count = 0;
		return temp;
	}
	else
	{
		count--;
		return array_[front++];
	}
}