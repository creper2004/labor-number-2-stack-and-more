#pragma once
#include <exception>

class WrongQueueSize : public std::exception
{
private:
	const char* reason;
public:
	WrongQueueSize();
	const char* what() const;
};