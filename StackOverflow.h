#pragma once
#include <exception>

class StackOverflow : public std::exception
{
private:
	const char* reason;
public:
	StackOverflow();
	const char* what() const;
};